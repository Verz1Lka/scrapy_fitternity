# -*- coding: utf-8 -*-

from scrapy.conf import settings
from scrapy.contrib.exporter import CsvItemExporter


class CSVkwItemExporter(CsvItemExporter):

    def __init__(self, *args, **kwargs):
        fields_to_export = ['name', 'id', 'link', 'country', 'city', 'area', 'category', 'phone', 'logo', 'address',
                            'lng', 'lat', 'email', 'timing', 'membership', 'sunday', 'monday', 'tuesday', 'wednesday',
                            'thursday', 'friday', 'saturday']
        kwargs['fields_to_export'] = fields_to_export or None
        kwargs['encoding'] = settings.get('EXPORT_ENCODING', 'utf-8')

        super(CSVkwItemExporter, self).__init__(*args, **kwargs)