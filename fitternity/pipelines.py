# -*- coding: utf-8 -*-
import MySQLdb
import time
# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy import signals
from scrapy.exporters import CsvItemExporter


class FitternityPipeline(object):
    
    def process_item(self, item, spider):
        if item['type'] == 'bus':
            del item['type']
            query = 'INSERT INTO m_business_new ('
            for key in item:
                query += key + ","
            query = query[:-1] + ") VALUES ('"
            for key in item:
                query += unicode(item[key]).replace("'","''").replace('\\','\\\\') + "','"
            query = query[:-2] + ')'
            try:
                spider.cur.execute(query)
                spider.conn.commit()
            except MySQLdb.Error as e:
                print query
                print e
            return item
        if item['type'] == 'ven':
            del item['type']
            query = 'INSERT INTO m_venue_new ('
            for key in item:
                query += key + ","
            query = query[:-1] + ") VALUES ('"
            for key in item:
                query += unicode(item[key]).replace("'","''") + "','"
            query = query[:-2] + ')'
            try:
                spider.cur.execute(query)
                spider.conn.commit()
            except MySQLdb.Error as e:
                print query
                print e
            return item
        if item['type'] == 'class': 
            del item['type']
            query = 'INSERT INTO m_class_new ('
            for key in item:
                query += key + ","
            query = query[:-1] + ") VALUES ('"
            for key in item:
                query += unicode(item[key]).replace("'","''") + "','"
            query = query[:-2] + ')'
            try:
                spider.cur.execute(query)
                spider.conn.commit()
            except MySQLdb.Error as e:
                print query
                print e
            return item
