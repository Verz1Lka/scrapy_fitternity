# -*- coding: utf-8 -*-

import MySQLdb
from scrapy.conf import settings


def get_biz_Id(self, url):
    query = "SELECT busi_id_pk from m_business_new where sourceUrl = '" + url + "'"
    self.cur.execute(query)
    rows = self.cur.fetchall()
#    print rows
    if len(rows) == 0:
        return 0
    id = rows[0][0]
    return id


def get_ven_id(self, url, area):
    area = area.replace("'", "''")
    query = "SELECT venue_id_pk from m_venue_new where sourceUrl = '" + url + "' and area like '%" + area + "%'"
#    print query
    try:
        self.cur.execute(query)
    except MySQLdb.Error as e:
        print query
        print e
    rows = self.cur.fetchall()
    if len(rows) == 0:
        return 0
    id = rows[0][0]
    return id


def clear_class(self, id):
    query = 'DELETE FROM m_class_new where busi_id_fk = ' + str(id)
    try:
        self.cur.execute(query)
        self.conn.commit()
    except MySQLdb.Error as e:
        print query
        print e
    return
