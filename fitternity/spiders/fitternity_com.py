# -*- coding: utf-8 -*-
import scrapy
import json
import datetime
import MySQLdb
from fitternity.items import FitternityItem, FitternityClassItem, FitternityVenItem
from fitternity.functions import get_biz_Id, clear_class, get_ven_id


class FitternityComSpider(scrapy.Spider):
    name = "fitternity.com"
    allowed_domains = ["fitternity.com"]
    start_urls = (
        'http://www.fitternity.com/capoeira-india',
    )
    conn = MySQLdb.connect(host="localhost", port=3306, user="root", passwd="fxlolpro", db="fx_test", charset='utf8')
    cur = conn.cursor()
    cur.execute('SET NAMES UTF8')
    
    def start_requests(self):
        cities = ['delhi', 'bangalore', 'mumbai', 'pune']
        for city in cities:
            body = '{"city":"' + city + '","from":0,"category":"","location":"","offerings":"","facilities":"",' \
                                        '"price_range":"","size":10000,"city_id":1}'
            rq = scrapy.Request('http://a1.fitternity.com/findersearch', method='POST', body=body)
            yield rq
        return

    def parse(self, response):
        jdata = json.loads(response.body_as_unicode())['search_results']['hits']['hits']
        for jdat in jdata:
            jdat = jdat['_source']
            link = 'http://a1.fitternity.com/finderdetail/' + jdat['slug']
            rq = scrapy.Request(link, callback=self.getData)
            yield rq
        return
    
    def getData(self, response):
        link = response.url
        jdata = json.loads(response.body_as_unicode())
        if jdata['statusfinder'] != 200:
            return
        jdat = jdata['finder']
        if get_biz_Id(self, link) == 0:
            itmBiz = FitternityItem()
            itmBiz['type'] = 'bus'
            itmBiz['business_name'] = jdat['title']
            itmBiz['city'] = jdat['city']['name']
            itmBiz['area'] = jdat['location']['name']
            itmBiz['phone_no'] = jdat['contact']['phone']
            itmBiz['Logo_Image'] = 'l/' + jdat['logo']
            itmBiz['Photo_Folder_Name'] = 'http://b.fitn.in/f/'
            itmBiz['Business_Cover_Image'] = 'c/' + jdat['coverimage']
            itmBiz['Longitude']= jdat['lon']
            itmBiz['Latitude']= jdat['lat']
            itmBiz['About'] = ' '.join(scrapy.Selector(text = jdat['info']['about']).xpath('//*/text()').extract())
            itmBiz['Additional_information'] = \
                ' '.join(scrapy.Selector(text=jdat['info']['additional_info']).xpath('//*/text()').extract())
            itmBiz['email'] = jdat['contact']['email']
            if 'finder_vcc_email' in jdat:
                itmBiz['email_2'] = jdat['finder_vcc_email']
            itmBiz['sourceUrl'] = response.url
            for f in jdat['facilities']:
                if f == 'free trial':
                    itmBiz['Free_Trial'] = 1
                if f == 'personal training':
                    itmBiz['Personal_Training'] = 1
                if f == 'group classes':
                    itmBiz['Group_Classes'] = 1
                if f == 'parking':
                    itmBiz['Parking'] = 1
            yield itmBiz
        else:
            pass
        if get_ven_id(self, link, '') == 0:
            venues = []
            tvenues = scrapy.Selector(text=jdat['contact']['address']).xpath('//p')
            for tven in tvenues:
                ven = tven.xpath('.//text()').extract()
                if len(ven) > 0:
                    if len(ven) > 1:
                        venues.append(ven)
                    else:
                        if len(ven[0].split()) == 0:
                            continue
                        tv1 = ven[0].split()[0]
                        tv2 = ' '.join(ven[0].split()[1:])
                        venues.append([tv1, tv2])
            for venue in venues:
                biz_id = get_biz_Id(self, response.url)
                itm_ven = FitternityVenItem()
                itm_ven['type'] = 'ven'
                itm_ven['busi_id_fk'] = biz_id
                itm_ven['sourceUrl'] = response.url
                itm_ven['area'] = jdat['location']['name']
                if len(venues) > 1:
                    itm_ven['area'] = venue[0].replace(':', '').strip()
                    itm_ven['address1'] = venue[1].strip()
                else:
                    itm_ven['address1'] = venue[0] + venue[1]
                itm_ven['venue_name'] = jdat['title']
                itm_ven['venue_logo'] =  'l/' + jdat['logo']
                itm_ven['venue_logo_folder_name'] = 'http://b.fitn.in/f/'
                for f in jdat['facilities']:
                    if f == 'parking':
                        itm_ven['parking'] = 1
                yield itm_ven
        else:
            pass
        clear_class(self, get_biz_Id(self, response.url))
        for cla in jdat['services']:
            tarea  = cla['name'].split(' at ')
            if len(tarea) == 1:
                area = ''
            else:
                area = tarea[1].split()[0]
            biz_id = get_biz_Id(self, response.url)
            venId = get_ven_id(self, response.url, area)
            if venId == 0:
                itm_ven = FitternityVenItem()
                itm_ven['busi_id_fk'] = biz_id
                itm_ven['sourceUrl'] = response.url
                itm_ven['area'] = tarea[-1]
                itm_ven['type'] = 'ven'
                yield itm_ven
                venId = get_ven_id(self, response.url, area)
            itm_class = FitternityClassItem()
            itm_class['busi_id_fk'] = biz_id
            itm_class['venue_id_fk'] = venId
            itm_class['type'] = 'class'
            itm_class['class_name'] = cla['name']
            if len(jdat['photos']) > 0:
                imageurl = 'http://b.fitn.in/f/g/full/'
                itm_class['class_photo_folder'] = imageurl + ['/'.join(photo['url'].split('/')[:-1]) for photo in
                                                              jdat['photos']][0]
                itm_class['class_photo'] = '; '.join([photo['url'].split('/')[-1] for photo in jdat['photos']])
            for cl in cla['workoutsessionschedules']:    
                for c in cl['slots']:
                    if len(c) == 1:
                        print '22'
                        print cl['slots'][c]
                        c = cl['slots'][c]
                        print c['end_time_24_hour_format']
                    tend = c['end_time_24_hour_format'].split('.')
                    tstart = c['start_time_24_hour_format'].split('.')
                    if len(tend) == 1:
                        tend.append('0')
                    if len(tstart) == 1:
                        tstart.append('0')
                    ta = datetime.datetime.strptime(c['slot_time'].split('-')[0].replace('am', 'AM').
                                                    replace('pm', 'PM'), '%I:%M %p')
                    tb = datetime.datetime.strptime(c['slot_time'].split('-')[1].replace('am', 'AM').
                                                    replace('pm', 'PM'), '%I:%M %p')
                    tl = str(tb - ta)[:-3]
                    itm_class['class_duration'] = tl
                    itm_class['price'] = c['price']
                    try:
                        itm_class[cl['weekday']] = '; '.join([slot['slot_time'] for slot in cl['slots']])
                    except IndexError:
                        print 'skip: ' + response.url
                    break
            yield itm_class
        return
