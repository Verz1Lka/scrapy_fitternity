# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class FitternityItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    type = scrapy.Field()
    business_name = scrapy.Field()
    area = scrapy.Field()
    city = scrapy.Field()
    PostCode = scrapy.Field()
    phone_no = scrapy.Field()
    Photo_Folder_Name = scrapy.Field()
    Logo_Image = scrapy.Field()
    Business_Cover_Image = scrapy.Field()
    About = scrapy.Field()
    Additional_information = scrapy.Field()
    Longitude = scrapy.Field()
    Latitude = scrapy.Field()
    Free_Trial = scrapy.Field()
    Personal_Training = scrapy.Field()
    Group_Classes = scrapy.Field()
    Parking = scrapy.Field()
    email = scrapy.Field()
    email_2 = scrapy.Field()
    email_3 = scrapy.Field()
    email_4 = scrapy.Field()
    sourceUrl = scrapy.Field()


class FitternityVenItem(scrapy.Item):
    type = scrapy.Field()
    busi_id_fk = scrapy.Field()
    venue_name = scrapy.Field()
    address1 = scrapy.Field()
    area = scrapy.Field()
    zip_code = scrapy.Field()
    Latitude = scrapy.Field()
    Longitude = scrapy.Field()
    shower = scrapy.Field()
    locker = scrapy.Field()
    ac = scrapy.Field()
    parking = scrapy.Field()
    parking_desc = scrapy.Field()
    venue_status = scrapy.Field()
    about_venue = scrapy.Field()
    venue_logo = scrapy.Field()
    venue_logo_folder_name = scrapy.Field()
    sourceUrl = scrapy.Field()


class FitternityClassItem(scrapy.Item):
    type = scrapy.Field()
    busi_id_fk = scrapy.Field()
    venue_id_fk = scrapy.Field()
    class_Category = scrapy.Field()
    instructor_fname = scrapy.Field()
    instructor_lname = scrapy.Field()
    instructor_email = scrapy.Field()
    class_name = scrapy.Field()
    class_gender = scrapy.Field()
    class_duration = scrapy.Field()
    class_photo = scrapy.Field()
    class_photo_folder = scrapy.Field()
    about_class = scrapy.Field()
    class_int_level = scrapy.Field()
    class_sweat_level = scrapy.Field()
    price = scrapy.Field()
    what_to_bring = scrapy.Field()
    weekday = scrapy.Field()
    monday = scrapy.Field()
    tuesday = scrapy.Field()
    wednesday = scrapy.Field()
    thursday = scrapy.Field()
    friday = scrapy.Field()
    saturday = scrapy.Field()
    sunday = scrapy.Field()